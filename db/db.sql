
create table Movie(
    movie_id integer primary key auto_increment,
    movie_title varchar(100),
    movie_release_date DATE,
    movie_time TIME,
    director_name varchar(100)
)