const express = require('express');

const db = require('../db');

const utils = require('../utils');

const router = express.Router();

//movie_id, movie_title, movie_release_date,movie_time,director_name


// 1. GET  --> Display Movie using name from Containerized MySQL

// 2. POST --> ADD Movie data into Containerized MySQL table

// 3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

// 4. DELETE --> Delete Movie from Containerized MySQL

router.post("/addMovie",(request, response)=>{
    const { movie_title, movie_release_date, movie_time, director_name } = request.body;

    const query = `insert into Movie values 
    ('0', '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')`;

    db.execute(query,(error, result)=>{
        response.send(utils.createResult(error, result));
    })

})

router.get("/getMovie/:movie_title",(request, response)=>{

    const { movie_title } = request.params
    // const { movie_title, movie_release_date, movie_time, director_name } = request.body;

    const query = `select * from Movie where movie_title = ${movie_title}`;

    db.execute(query,(error, result)=>{
        response.send(utils.createResult(error, result));
    })

})

router.put("/updateMovie/:id",(request, response)=>{
    const { id } = request.params
    const { movie_release_date, movie_time } = request.body;

    const query = `
    UPDATE Movie
    SET
    movie_release_date = '${movie_release_date}', 
    movie_time = '${movie_time}'
    WHERE
    movie_id = ${id}
  `

    db.execute(query,(error, result)=>{
        response.send(utils.createResult(error, result));
    })

})

router.delete("/deleteMovie/:id",(request, response)=>{
    const { id } = request.params

    const query = `delete from Movie where movie_id='${id}'`;

    db.execute(query,(error, result)=>{
        response.send(utils.createResult(error, result));
    })

})

module.exports = router;